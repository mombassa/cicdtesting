import os
import re

os.system ("git clone https://gitlab.com/mombassa/ci_readme_repo/")
os.chdir ("ci_readme_repo")

with open('build_url.txt', 'r') as file:
    url_data = file.readlines()

build_os = {"WINDOWS", "LINUX", "macOS"}
os_url_map = {}

for i in build_os:
    for line in url_data:
        st = line.split()
        if i in line:
            os_url_map[i] = st[1]
            break

print os_url_map
os.chdir ("..")


with open('README.md', 'r') as file:
    data = file.readlines()

for index in range(0, len(data)):
    line = data[index]
    token = re.findall(r'\([^()]*\)',line )
    for i in build_os:
        if i in line:
            line = line.replace (token[1], "(" + os_url_map[i] + ")")
            data[index] = line
            break

with open('README.md', 'w') as file:
    file.writelines( data )