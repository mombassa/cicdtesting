from conans import CMake,ConanFile
import os
class Features(ConanFile):

    name = "Features"
    version = "1.0"
    settings = "os","arch","compiler","build_type"
    generators = "cmake"
    build_policy = "missing"
    exports_sources = "*"
    def build_requirements(self):
       self.build_requires("gtest/1.8.1@zoho/testing")

    def build(self):
       cmake = CMake(self)
       cmake.configure()
       cmake.build()
       build_dir_for_test = os.path.join(self.build_folder,"bin")
       self.output.info(build_dir_for_test)
       if "CI_PROJECT_DIR" in os.environ:
           with open(os.environ['CI_PROJECT_DIR'] + "/conan_path.txt", "w+") as file:
               print ("writing file")
               file.write (self.build_folder)

    def package(self):
        if self.settings.compiler == "emscripten":
            self.copy("*Wasm.wasm",dst="lib",keep_path=False)
            self.copy("*Wasm.js",dst="lib",keep_path=False)
            self.copy("*Wasm.wasm",dst=self.cm_build_output_lib,keep_path=False)
            self.copy("*Wasm.js",dst=self.cm_build_output_lib,keep_path=False)


        else:
            self.copy("*.hpp", dst="include",excludes="generated")
            self.copy("*.lib", dst="lib", keep_path=False)
            self.copy("*.dll", dst="bin", keep_path=False)
            self.copy("*.so", dst="lib", keep_path=False)
            self.copy("*.dylib", dst="lib", keep_path=False)
            self.copy("*.a", dst="lib", keep_path=False)

            self.copy("*.hpp",excludes="generated")
            if self.settings.compiler=="Visual Studio":
                self.copy("*.lib", keep_path=False)
            else:
                self.copy("*.lib", keep_path=False)
            self.copy("*.dll", keep_path=False)
            self.copy("*.so", keep_path=False)
            self.copy("*.dylib",  keep_path=False)
            self.copy("*.a",  keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["features"]

