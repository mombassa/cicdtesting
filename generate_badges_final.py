import os
import gitlab

BADGE_NAME=os.environ['CI_JOB_NAME']
BADGE_STATUS = ""
os.system ("git clone https://gitlab.com/mombassa/badges_folder.git")
os.chdir("badges_folder")

st = "git remote set-url origin https://gitlab-ci-token:"
st += os.environ['CI_PUSH_TOKEN']
st += "@gitlab.com/mombassa/badges_folder.git/"
os.system (st)

os.system ("git config --global user.email 'sankaryan96@gmail.com'")
os.system ("git config --global user.name 'mombassa'")

url = 'https://gitlab.com'
gl = gitlab.Gitlab(url, os.environ['CI_PUSH_TOKEN'], api_version=4)
project = gl.projects.get(os.environ['CI_PROJECT_ID'], lazy=True)
last_pipeline = project.pipelines.get (os.environ['CI_PIPELINE_ID'])
jobs = last_pipeline.jobs.list()

job_names = ["LINUX_x86_64" , "macOS_x86_64", "WINDOWS_x86_64"]

for j in jobs:
    if j.name in job_names:
        REQUIRED_BADGE=j.name+"-"+j.status+".svg"
        os.system("cp "+REQUIRED_BADGE+" "+j.name+".svg")
        os.system("git add "+j.name+".svg")

os.system ("git commit -m \"[skip ci] commit from CI runner\"")
os.system ("git push origin master")
os.chdir ("..")
os.system ("rm -rf badges_folder")



