import os
import sys

f=open("conan_path.txt", "r")
conan_path=f.read()

package_folder = conan_path.replace ("build", "package")

directory = os.environ['CI_JOB_NAME']
os.system ("mkdir " + directory )
if os.environ['CI_JOB_NAME'] == "WINDOWS_x86_64":
    os.system ("cp -a " + package_folder + "\\" + " " + directory + "\\" )
else:
    os.system ("cp -R " + package_folder + "/include" + " " + directory + "/")
    os.system ("cp -R " + package_folder + "/lib" + " " + directory + "/")