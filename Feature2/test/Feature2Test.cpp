//
// Created by Sankaranarayanan G on 2019-01-29.
//

#include <gtest/gtest.h>

#include <Feature2/Feature2.hpp>

class Feature2Test : public ::testing::Test {
  protected:
    void SetUp () override {
    }
    Feature2 functions;
};


TEST_F (Feature2Test, Feature2Test_FIBONACCI) {
    EXPECT_EQ (functions.Fibonacci (1), 0);
    EXPECT_EQ (functions.Fibonacci (2), 1);
    EXPECT_EQ (functions.Fibonacci (3), 1);
    EXPECT_EQ (functions.Fibonacci (4), 2);
    EXPECT_EQ (functions.Fibonacci (5), 3);
}

TEST_F (Feature2Test, Feature2Test_GCD) {
    EXPECT_EQ (functions.GCD (2, 4), 2);
    EXPECT_EQ (functions.GCD (2, 3), 1);
    EXPECT_EQ (functions.GCD (4, 6), 2);
    EXPECT_EQ (functions.GCD (45, 55), 5);
    EXPECT_EQ (functions.GCD (1, 4), 1);
}
