import gitlab
import os
import re


url = 'https://gitlab.com'

os.system ("git clone https://gitlab.com/mombassa/badges_folder.git")
os.chdir("badges_folder")

job_names = ["LINUX_x86_64" , "macOS_x86_64", "WINDOWS_x86_64"]
job_names_url_map = {}
final_job_names = []


# Get all the job URLS
gl = gitlab.Gitlab(url, os.environ['CI_PUSH_TOKEN'], api_version=4)
project = gl.projects.get(os.environ['CI_PROJECT_ID'], lazy=True)
last_pipeline = project.pipelines.get (os.environ['CI_PIPELINE_ID'])
jobs = last_pipeline.jobs.list()


# Generate the "running" badges for all the platforms

st = "git remote set-url origin https://gitlab-ci-token:"
st += os.environ['CI_PUSH_TOKEN']
st += "@gitlab.com/mombassa/badges_folder.git/"
os.system (st)
os.system ("git config --global user.email 'sankaryan96@gmail.com'")
os.system ("git config --global user.name 'mombassa'")

for j in jobs:
    if j.name in job_names:
        os.system("cp "+j.name+"-running.svg "+j.name+".svg")
        os.system ("git add "+j.name+".svg")
        job_names_url_map[j.name] = j.web_url
        final_job_names.append (j.name)

os.system("ls")
os.system ("git commit -m \"[skip ci] commit\"")
os.system ("git push origin master")

os.chdir("..")
os.system("pwd")


# Update the README.md file with the obtained URLs
with open('README.md', 'r') as file:
    data = file.readlines()

for index in range(0, len(data)):
    line = data[index]
    token = re.findall(r'\([^()]*\)',line )
    for i in final_job_names:
        if i in line:
            line = line.replace (token[1], "(" + job_names_url_map[i] + ")")
            data[index] = line
            break

with open('README.md', 'w') as file:
    file.writelines( data )

