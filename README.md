[![Coverage report](https://gitlab.com/mombassa/cicdtesting/badges/master/coverage.svg)](https://mombassa.gitlab.io/cicdtesting/)


| __PLATFORM__     | __STATUS__ |
|:-------:|:-------:|
|__WINDOWS_x86_64__|[![badge](https://gitlab.com/mombassa/badges_folder/raw/master/WINDOWS_x86_64.svg)](https://gitlab.com/mombassa/cicdtesting/-/jobs/261289796)|
|__LINUX_x86_64__|[![badge](https://gitlab.com/mombassa/badges_folder/raw/master/LINUX_x86_64.svg)](https://gitlab.com/mombassa/cicdtesting/-/jobs/261213600)|
|__macOS_x86_64__|[![badge](https://gitlab.com/mombassa/badges_folder/raw/master/macOS_x86_64.svg)](https://gitlab.com/mombassa/cicdtesting/-/jobs/261213624)|