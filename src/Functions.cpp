//
// Created by Sankaranarayanan G on 2019-01-29.
//

#include "Functions.hpp"
#include <algorithm>

using namespace std;

long long int Functions::Fibonacci (int n) {
    long long arr[n + 2];
    arr[1] = 0;
    arr[2] = 1;

    for (int i = 3; i <= n; i++) {
        arr[i] = arr[i - 1] + arr[i - 2];
    }
    return arr[n];
}

int Functions::GCD (int a, int b) {

    while (b != 0) {
        int temp = b;
        b = a % b;
        a = temp;
    }
    return a;
}
