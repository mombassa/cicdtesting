//
// Created by Sankaranarayanan G on 2019-01-29.
//

#ifndef CICDTESTING_FUNCTIONS_HPP
#define CICDTESTING_FUNCTIONS_HPP

class Functions {
  public:
    long long int Fibonacci (int n);

    int GCD (int a, int b);
};
#endif // CICDTESTING_FUNCTIONS_HPP
